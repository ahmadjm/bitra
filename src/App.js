import "./App.css";
import "antd/dist/antd.css";
import { Layout } from "antd";

import { RTLLayoout } from "./components/styled";
import { Header as PageHeader } from "./components/header";
import { Slider } from "./components/slider";
import { Introduction } from "./components/introduction";
import {Features} from "./components/features";
import {AminToken} from "./components/aminToken";
import {RoadMap} from "./components/roadMap";
import {Coleagues} from "./components/coleagues";
import {Founders} from "./components/founders";
import {Events} from "./components/events";
import {BitraFooter} from "./components/bitraFooter";
import {SignUp} from "./components/signUp";
function App() {
  const { Content } = Layout;
  return (
    <div className="App">
      <RTLLayoout>
        <PageHeader />
        <Content style={{paddingTop:"66px"}} >
          <Slider />
          <Introduction/>
          <Features />          
          <SignUp /> 
          <AminToken />          
          <RoadMap />          
          <Coleagues /> 
          <Founders />
          <Events />   
        </Content>        
        <BitraFooter /> 
       </RTLLayoout>
    </div>
  );
}

export default App;
