import React from "react";
import { Row, Card,Col } from "antd";
import { FeatHeader, RTLCard, ModuleContainer } from "../components/styled";
import areaTaklogo from "../assets/img/areaTaklogo.png";
import radeLogo from "../assets/img/radeLogo.png";



export const mainStyle = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
};
export const Coleagues = () => {

  return (
    <div>
      <FeatHeader>همکاران ما </FeatHeader>
      <RTLCard hoverable={false} bordered={false}>
        <ModuleContainer>          
            <Row gutter={12} style={{ margin: "0" }}  className={'flipView'}  >
              <Col span={8}>
                <img
                  src={areaTaklogo}
                  alt={"اریاتک"}
                  width={250}
                />
                <Card title="اریاتک" bordered={false} style={{justifyContent : "center"}} ></Card>
              </Col>
              <Col span={8}>
              <img
                src={radeLogo}
                alt={"رده"}
                width={250}
              />
              <Card title="رده" bordered={false} style={{justifyContent : "center"}} ></Card>
              </Col>
            </Row>
        </ModuleContainer>
      </RTLCard>
      </div>
  );
};
