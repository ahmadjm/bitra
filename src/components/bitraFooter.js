import React from "react";
import { Card, Row, Col } from "antd";
import {  ModuleContainer, FeatHeader ,RTLCard } from "./styled";
import HomeIcon from "@material-ui/icons/Home";
import CallIcon from "@material-ui/icons/Call";
import InstagramIcon from "@material-ui/icons/Instagram";
import HttpIcon from "@material-ui/icons/Http";
import TwitterIcon from "@material-ui/icons/Twitter";
import TelegramIcon from "@material-ui/icons/Telegram";
import aparat from "../assets/img/aparat.png";
export const BitraFooter = () => {
  return (
        
    <RTLCard hoverable={false} bordered={false}  style={{background : "#435469"}} >                      
        <ModuleContainer style={{textAlign: "center"}} >
          <Row gutter={24} style={{ margin: "0" }}  className={'flipView'} >
            <Col span={12}  >
            <FeatHeader style={{background: "#435469",borderBottom : "1px solid black",marginBottom : "23px",fontSize :"16px"}}  > بازار یکپارچه تبادل رمزارز امین (بیترا)</FeatHeader>
                <ul style={{  paddingRight: "0px",listStyleType: "none",textAlign :"right",fontSize : "14px",minWidth:"350px" }}>
                  <li >
                    <HomeIcon style={{fontSize : "14px"}} />
                   خیابان شریعتی، کوچه حسینه ارشاد، پلاک 4، واحد 2
                  </li>
                  <li>
                    <CallIcon style={{fontSize : "14px"}} /> 021-22894173
                  </li>
                  <li>
                    <HttpIcon style={{fontSize : "14px"}} /> bitramarket@gmail.com
                  </li>
                </ul>
            </Col>
            <Col span={12}  >
            <FeatHeader style={{background: "#435469",borderBottom : "1px solid black",marginBottom : "23px",fontSize :"16px"}} > بیترا را در شبکه‌های اجتماعی دنبال کنید</FeatHeader>
            <ul style={{listStyleType: "none",margin :"auto",fontSize : "12px" }}>
              <li>
                <a href={'https://t.me/bitraofficial'} ><TelegramIcon /></a>
                <a href={'https://www.instagram.com/accounts/login/?next=/bitra.official/'} ><InstagramIcon /></a>
                <a href={'https://mobile.twitter.com/BitraOfficial'} ><TwitterIcon /></a>
                <a href={'https://www.aparat.com/Bitra_official'} ><img src={aparat} alt={'آپارات'} style={{width:"28px",marginBottom:"12px"}} /></a>
              </li>
            </ul>
            </Col>
          </Row>    
        </ModuleContainer>
    </RTLCard>      
  )  
};
