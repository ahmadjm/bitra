import React, { useState } from "react";
import { Popover, Button, Input } from "antd";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

export const Login = () => {
  const [visible, setVisible] = useState(false);
  const hide = () => {
    setVisible(false);
  };

  const handleVisibleChange = (visible) => {
    setVisible(visible);
  };
  return (
    <Popover
      content={
        <table style={{ direction: "rtl" }} cellPadding={5}>
          <tr>
            <td>
              <Input placeholder={"نام و نام خانوادگی"} />
            </td>
          </tr>
          <tr>
            <td>
              <Input placeholder={"ایمیل"} />
            </td>
          </tr>
          <tr>
            <td>
              <Input placeholder={"تلفن همراه"} />
            </td>
          </tr>
          <tr>
            <td>
              <Button type={"primary"}>عضویت</Button>
            </td>
          </tr>
        </table>
      }
      title=""
      trigger="click"
      visible={visible}
      onVisibleChange={handleVisibleChange}      
    >
        <div
          type="ghost"
          style={{
            color: "white",
            border: "none",
            marginTop: "10px",
            cursor: "pointer",
          }}
        >
        <AccountCircleIcon fontSize={"large"}  className={'userLogin'} />
      </div>
    </Popover>
  );
};
