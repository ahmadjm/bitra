import React from "react";
import {  LoginForm, Header, LoginContainer } from "./styled";
import MailchimpSubscribe from "react-mailchimp-subscribe"
import {MyForm} from "./mailChimpForm";
import { notification ,Spin } from 'antd';

export const SignUp = () => {  
  var myDescription = "";
  const openNotification = (description,myStatus) => {    
    if(myDescription !== description){
      myDescription = description;
      notification.open({
        message: myStatus === 1 ?  <span style={{color : "green"}} > عضویت در بیترا</span>: <span style={{color : "red"}} >خطا</span>,
        description: description,
        duration : 4.5,        
        className: 'custom-class',
        style: {
          width: 220,
        },
      });    
  }};
  return(
    <LoginContainer id={'userLogin'} >
      <LoginForm hoverable={false} bordered={false}   style={{padding: "55px 0 "}}  >
        <Header>
          <p> با پیش ثبت نام ،  رایگان معامله کنید.</p>
        </Header>
        <MailchimpSubscribe 
          url={process.env.REACT_APP_MAILCHIMP_URL} 
          render={({ subscribe, status, message })=>(
            <div style={{position : "relative"}}>
              <MyForm  onValidated={formData => subscribe(formData)} />
              {status === "sending" && <Spin size={'small'} style={{position : "absolute",top:"65px"}}  />}
              {status === "error" && openNotification( "ایمیل قبلا ثبت نام شده است" , 0  )}
              {status === "success" && openNotification( " با موفقیت انجام پذیرفت " , 1 )}
            </div>
        )}
      />
      </LoginForm>
      </LoginContainer>

  );
};
