import styled from "styled-components";


export const Artboard = styled.div `
& .cards {
	display: -webkit-flex;
	display: flex;
	-webkit-justify-content: center;
	justify-content: center;
	-webkit-flex-wrap: wrap;
	flex-wrap: wrap;
	margin-top: 15px;
	padding: 1.5%;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
& .card {
	position: relative;
	margin-bottom: 20px; 
	padding-bottom: 30px; 
	background: #fefff9;
	color: #363636; 
	text-decoration: none;
	box-shadow(rgba(black, 0.19) 0 0 8px 0);
	border-radius(4px);
	breakpoint(700px, $no-query: true) {
	//switch to 2 columns
		width: 320px;
		margin-right: 20px;
		margin-bottom: 20px;
		&:nth-child(even) {
			margin-right: 0;
		}
	}
    span {
		display: block;
	}
	.card-summary {
        padding: 5% 5% 3% 5%;
        
	}
	.card-header {
		position: relative;
		height: 375px;
		overflow: hidden;
		background-repeat: no-repeat;
		background-size: cover;
		background-position: center;
		background-color: rgba(white,.15);
 		background-blend-mode: overlay;
		@include border-radius(4px 4px 0 0);
		&:hover, &:focus {
			background-color: rgba(white, 0);
		}
	}
	.card-title {
		background: #FBBB04;
		padding: 3.5% 0 2.5% 0;
		color: white;
		font-family: 'Roboto Condensed', sans-serif;
		text-transform: uppercase;
		position: absolute;
		bottom: 0;
		width: 100%;
		h3 {
			font-size: 1.2em;
			line-height: 1.2;
			padding: 0 3.5%;
			margin: 0;
		}
	}
	.card-meta {
		max-height: 0;
		overflow: hidden;
		color: #666;
		font-size: .78em;
		text-transform: uppercase;
		position: absolute;
		bottom: 5%;
		padding: 0 5%;
		transition-property(max-height);
		transition-duration(.4s);
		transition-timing-function(ease-in-out);
	}
	&:hover, &:focus {
		background: white;
	    box-shadow(rgba(black, 0.45) 0px 0px 20px 0px);
		.card-title {
			background: rgba(157, 187, 63, .95);
		}
		.card-meta {
			max-height: 1em;
		}
	}
}
img {
    max-width: 100%; 
}


`

 