import React from "react";
import {Artboard} from "./style";
export const BitraCard = (props)=>{
    const {imgSrc,title,description,width} = props;

    return (
        <Artboard>
            <div className={"cards"}>
                <a className={"card"}  style={{width:width+"px",cursor:"auto"}} >
                    <span className={"card-header"}  style={{backgroundImage : `url(`+imgSrc+`)`,backgroundSize: "cover",width: "340px",height: "350px" }} >
                        <span className={"card-title"}>
                            <h3>{title}</h3>
                        </span>
                    </span>
                    <span className={"card-summary"}>
                        {description}
                    </span>
                </a>
            </div>
        </Artboard>
    )
}
