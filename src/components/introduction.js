import React from "react";
import {  Col, Row,Button } from "antd";
import { RTLCard } from "./styled";
import logo from "../assets/img/icons/illustration.svg";
import { ModuleContainer, Header } from "../components/styled";
import { DownloadOutlined } from "@ant-design/icons";

export const Introduction = () => {
  return (
    <RTLCard hoverable={false} bordered={false}>
      <ModuleContainer>
        <Row style={{ margin: "0" }} className={"flipView"}>
          <Col hoverable={false} span={6} style={{ textAlign: "center" }}>
            <img alt="توکن امین" src={logo} className={"sideImg"} />
          </Col>
          <Col hoverable={false} span={12}>
            <Header>بازار یکپارچه تبادل رمز‌ارز امین</Header>
            <div style={{ textAlign: "right" }} className={"sideTxt"}>
              <p>
                بازار یکپارچه تبادل رمزارز امین (بیترا) با چشم‌انداز خلق یک
                کارگزاری غیرمتمرکز رمزارزی پایه‌گذاری شده است
                بیترا محلی است برای تسهیل تبادلات رمزارزها با ایجاد محیط
                پیشنهاد‌گذاری و ایجاد درخواست‌های خرید و فروش توسط کاربران و
                مدیریت مچینگ این درخواست‌ها ; در مرحله پیش رو، این کارگزاری با
                تکیه بر قراردادهای حقوقی در چارچوب قوانین جمهوری اسلامی ایران و
                نیز با به‌کارگیری قابلیت‌های گواهی سرمایه گذاری دیجیتال (توکن‌)
                مبتنی بر بلاکچین به توزیع منافع حاصل از صرافی اقدام نموده است.
              </p>
              <div style={{display:"flex",flexDirection:"row",justifyContent:"flex-end"}} >
              <Button type="primary" icon={<DownloadOutlined />} size={"middle"} href="/bitra.pdf" download >
                معرفی بیترا
              </Button>
              </div>
            </div>
          </Col>
        </Row>
      </ModuleContainer>
    </RTLCard>
  );
};
