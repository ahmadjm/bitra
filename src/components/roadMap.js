import React from "react";
import roadMap from "../assets/img/road_map.png";

import {RTLCard,RoadmapContent,Header} from "./styled";
const contentStyle = {
    display: "flex",
    flexDirection : "column",
    justifyContent: 'space-around',
    padding : "55px 25px"
  };
export const RoadMap = ()=>{
    return(
     <RTLCard hoverable={false} bordered={false} style={contentStyle}  id={'mapElement'} >
      
      <div style={{display:'flex',flexDirection :"column",alignItems:"center",justifyContent:"center",width : "100%"}} >
        <Header style={{marginBottom : "3em"}} >
            نقشه راه        
        </Header>
        <RoadmapContent>
          <img src={roadMap} />
          <div className={'step0'} ><h4>1تیر 99</h4><p>تشکیل تیم بنیانگذاران</p></div>
          <div className={'step1'} ><h4>۱ شهریور 99</h4><p>تامین سرمایه اولیه</p></div>
          <div className={'step2'} ><h4>10 شهریور 99</h4><p>راند خصوصی</p></div>
          <div className={'step3'} ><h4>15 شهریور 99</h4><p>آغاز ساخت زیر ساخت فنی</p></div>
          <div className={'step4'} ><h4>1 مهر 99</h4><p>آغاز واگذاری راند اول</p></div>
          <div className={'step5'} ><h4>10 آذر 99</h4><p>آغاز تست اولیه</p></div>
          <div className={'step6'} ><h4></h4><p>افتتاحیه</p></div>
          <div className={'step7'} ><h4>فروردین 1400</h4><p>واگذاری راند دوم</p></div>
        </RoadmapContent>
      </div>
     </RTLCard>
    )
} 