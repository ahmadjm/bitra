import React from "react";
import { Header, ModuleContainer } from "./styled";
import {  Carousel } from "antd";
import { FlexForFlip } from "./styled";
import bitra from "../assets/img/logo.png";
export const Events = () => {
  return (
    <div
      style={{ background: "white", margin: "0", padding: "5px 0 0 0" }}
      id={"eventsElement"}
    >
      <Header>رویدادهای بیترا</Header>
      <Carousel autoplay effect={"fade"}>
        <div>
          <ModuleContainer>
            <FlexForFlip>
              <img alt="bitra" src={bitra} className={"image"} />
              <p>
                <h4>بیترا سهم تو از آینده</h4>
                <p>
                مجموعه بیترا برای آشنایی شما عزیزان با کارگزاری رمزارزی بیترا و گواهی سرمایه‌گذاری<br/>
                دیجیتالی (توکن) امین، در وبیناری با همراهی مهندس سعید خوشبخت،از بنیان‌گذاران<br/>
                 این کارگزاری به معرفی ویژگی‌ها و مزیت‌های این مجموعه خواهد پرداخت.
                </p>
                <p style={{textAlign : "center"}} >
                    <span>روزهای پنجشنبه ساعت ۱۴:۳۰ الی ۱۶ همراه شما خواهیم بود</span><br/>
                    <span>میهمان ما برای حضور در این وبینار باشید</span><br/>
                    <span><a href="https://cra-tc.petiak.ir/b/3kh-pyz-7jx" >https://cra-tc.petiak.ir</a></span>
                </p>
              </p>
            </FlexForFlip>
          </ModuleContainer>
        </div>
      </Carousel>
    </div>
  );
};
