import React,{useRef,useState} from "react";
import { Row, Col, Menu } from "antd";
import logo from "../assets/img/logo.png";
import logoW from "../assets/img/logo-white.png"
import {
  CalendarOutlined,
  AppstoreOutlined,
  HeatMapOutlined,
  UserAddOutlined,
} from "@ant-design/icons";


const handleClick = (e) => {
  var elmnt = document.getElementById(e.key);
  elmnt.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
};


export const Header = () => {
  const [mainLogo,setMainLogo] = useState(true);

  const navbar = useRef(null);
  window.onscroll = function() {myFunction()};
  function myFunction() {
    if(window.pageYOffset === 0){
      setMainLogo(true);
    }else{      
      setMainLogo(false);
    }    
  }
  return (
    <Row className={ mainLogo ? "sticky" : "sticky sticky4Slide"  }  ref={navbar}  >
      <Col span={12}>
      {mainLogo ? (
        <img src={logo} alt="بازار یکپارچه تبادل ارزامین" width={150} />
      ) : (
        <img src={logoW} alt="بازار یکپارچه تبادل ارزامین" width={150} />
      )}
      </Col>
      <Col
        span={12}
        style={{
          justifyContent: "flex-start",
          flexDirection: "row",
          display: "flex",
        }}
      >
        <Menu
          mode="horizontal"
          onClick={handleClick}
          style={{
            background: "transparent",
            border: "none",
            lineHeight: "65px",
          }}
        >
          <Menu.Item key="tokenElement" icon={<AppstoreOutlined />}>
            توکن امین
          </Menu.Item>
          <Menu.Item key="mapElement" icon={<HeatMapOutlined />}>
            نقشه راه
          </Menu.Item>
          <Menu.Item key="eventsElement" icon={<CalendarOutlined />}>
            رویدادها
          </Menu.Item>
          <Menu.Item key="userLogin" icon={<UserAddOutlined />}>
            عضویت
          </Menu.Item>
        </Menu>
      </Col>
    </Row>
  );
};
