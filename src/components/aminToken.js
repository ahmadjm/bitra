import React from "react";
import { Col, Row } from "antd";
import { AminCard, Header, ModuleContainer } from "./styled";
import AminTok from "../assets/img/aminTokenLogo.png";
export const AminToken = () => {
  return (
    <AminCard
      hoverable={false}
      bordered={false}
      style={{ justifyContent: "center" }}
      id={"tokenElement"}
    >
      <ModuleContainer>
        <Row
          gutter={24}
          style={{ margin: "0", justifyContent: "center" }}
          className={"flipView"}
        >
          <Col span={24} hoverable={false}>
            <Header style={{ textAlign: "right", marginBottom: "20px" }}>
              <img style={{width:"50px"}} alt="توکن امین" src={AminTok} />
              گواهی سرمایه‌گذاری دیجیتال (توکن) امین
            </Header>
            <p>
              گواهی سرمایه‌گذاری دیجیتال (توکن) امین گام اولیه در راستای محقق
              شدن هدف توزیع‌شدگی کسب‌وکار کارگزاری است. در این گام، منافع حاصل
              از فعالیت‌های کارگزاری که طبق گزارش‌های دوره‌ای به اطلاع عموم
              می‌رسد، در پایان هر دوره یک‌ساله مالی پس از انجام روال‌های قانونی
              و بررسی حسابرس رسمی و بازرس شرکت با توجه به سود خالص صرافی، میان
              مالکان گواهی سرمایه‌گذاری دیجیتال (توکن) امین در زمان ابلاغ‌شده،
              تقسیم می‌شود. هر هزارم درصد از منافع اکسچنج یک «واحد منفعت» تعریف
              می‌گردد که به‌صورت یک گواهی دیجیتال، که درواقع روی قرارداد هوشمندی
              بر روی شبکه استلار تحت عنوان « گواهی سرمایه‌گذاری دیجیتال (توکن)
              امین » تعریف شده است از آدرسی مشخص به سرمایه‌گذار منتقل می‌شود .
            </p>
          </Col>
        </Row>
      </ModuleContainer>
    </AminCard>
  );
};
