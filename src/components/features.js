import React from "react";
import { Card, Row, Col } from "antd";
import {  FeatHeader, RTLCard,ModuleContainer } from "./styled";

import decentralizing from "../assets/img/icons/1.svg";
import invest from "../assets/img/icons/2.svg";
import actor from "../assets/img/icons/3.svg";
import transparency from "../assets/img/icons/4.svg";

export const Features = () => {
  return (
    <>
  <FeatHeader> ویژگی‌ها و ارزش‌های ما </FeatHeader>
  <RTLCard hoverable={false} bordered={false}>                      
        <ModuleContainer style={{textAlign: "center"}} >
          <Row gutter={24} style={{ margin: "0" }}  className={'flipView'} >
            <Col span={6}  >
              <img
                src={decentralizing}
                alt={"غیر متمرکز سازی"}
                width={60}
                style={{ marginTop: "10px" }}
                title={"غیر متمرکز سازی"}
              />
              <Card title="غیر متمرکز سازی" bordered={false} style={{justifyContent : "center"}}  >
                <div style={{width : "250px"}} >
                غیرمتمرکز سازی در سه سطح توزیع منافع، توزیع قدرت تصمیم‌گیری و
                توزیع مالکیت زیرساخت
                </div>
              </Card>
            </Col>
            <Col span={6}>
              <img
                src={invest}
                width={60}
                alt={"با هر سرمایه ای صاحب منفعت باشید"}
                style={{ marginTop: "10px" }}
              />
              <Card title="با هر سرمایه ای صاحب منفعت باشید" bordered={false} style={{justifyContent : "center"}} >
              <div style={{width : "250px"}} >

                فراهم ساختن بستری است که بازار بالقوه معاملات رمزارز در ایران را
                بالفعل سازد تا این حوزه کسب‌وکار و سرمایه‌گذاری برای آحاد افراد
                و سازمان‌ها در ایران دسترس‌پذیر گردد{" "}
                </div>
              </Card>
            </Col>
            <Col span={6}>
              <img
                src={actor}
                width={60}
                alt={"بازیگران ارزشمند"}
                style={{ marginTop: "10px" }}
              />
              <Card title="بازیگران ارزشمند" bordered={false} style={{justifyContent : "center"}} >
              <div style={{width : "250px"}} >

                تبدیل بازار کوچک و کم‌ بازیگر معاملات رمزارزی، به بازاری گسترده
                و پرنفوذ
                </div>
              </Card>
            </Col>
            <Col span={6}>
              <img
                src={transparency}
                width={60}
                alt={"شفافیت"}
                style={{ marginTop: "10px" }}
              />
              <Card title="شفافیت" bordered={false} style={{justifyContent : "center"}} >
              <div style={{width : "250px"}} >

                فراهم ساختن دسترسی به اطلاعات قابل اتکا برای تصمیم‌گیری و راهکار
                آسان برای اجرای تصمیمات معامله و سرمایه‌گذاری در حوزه رمزارزها
                برای کاربران
              </div>
              </Card>
            </Col>
          </Row>
        </ModuleContainer>
    </RTLCard>
    </>
  );
};
