import React from "react";
import { Form } from "antd";
import { UserOutlined, MailOutlined, MobileOutlined } from "@ant-design/icons";
import {Bitrainput , BitraForm , BitraButton} from "../components/styled";

export const MyForm = ({onValidated})=>{ 
    const onFinish = (formData)=>{
        onValidated({
            EMAIL: formData.email,
            FNAME: formData.FName,
            PHONE: formData.mobile,
          });
    }   
    return(
    <BitraForm
          name="normal_login"
          className="login-form"        
          onFinish={onFinish}
        >
      <Form.Item
            name="FName"
            rules={[{ required: true, message: "لطفا نام ونام‌خانوادگی را وارد نمایید!" }]}
            
          >
        <Bitrainput
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder="نام و نام‌خانوادگی"
          className={'bitra-input'}
        />
      </Form.Item>
      <Form.Item
            name="email"
            rules={[{ required: true, message: "لطفا ایمیل را وارد نمایید!" }]}
          >
            <Bitrainput
              prefix={<MailOutlined className="site-form-item-icon" />}
              placeholder="ایمیل"
              className={'bitra-input'}
            />
        </Form.Item>
        <Form.Item
            name="mobile"
            rules={[{ required: true, message: "لطفا شماره موبایل خود را وارد نمایید!" }]}
          >
            <Bitrainput
              prefix={<MobileOutlined  className="site-form-item-icon" />}
              placeholder="تلفن همراه"
              className={'bitra-input'}
            />
          </Form.Item>
        <Form.Item style={{textAlign : "left"}} >
            <BitraButton
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              ثبت نام
            </BitraButton>
        </Form.Item>
      </BitraForm>
      )
    }