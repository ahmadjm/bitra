import React from "react";
import { Card, Col, Row } from 'antd';
import {RTLCard,FeatHeader,ModuleContainer} from "./styled";
import {BitraCard} from "../components/card/card"

const componentStyle = {
    display : "flex",
    flexDirection : "row",
    justifyContent : "space-between",
    aligItems : "center"  
}
export const mainStyle = {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
}

export const ShareDist = ()=>{
    return (
      <>
          <FeatHeader> راندهای واگذاری گواهی سرمایه گذاری دیجیتال (توکن) امین </FeatHeader>
          <RTLCard hoverable={false} bordered={false}>              
          <ModuleContainer>        
            <div className="site-card-wrapper" style={{mainStyle}}  > 
          <Row style={componentStyle}  >
            <Col span={6} >
              <BitraCard title="۱۰۸,۰۰۰ تومان" >
                <h3>راند خصوصی</h3>
                <p>1 الی 30 شهریور 1399</p>
                <p>قیمت : 108.000 تومان</p>
                <p>پایان واگذاری</p>
              </BitraCard>
            </Col>
            <Col span={6}>
              <Card title="۱۲۹,۰۰۰ تومان" >
                <h3>راند اول</h3>
                <p>1 الی 30 مهر 1399</p>
                <p>قیمت : 128.000 تومان</p>
                <p>پایان واگذاری</p>
              </Card>
            </Col>
            <Col span={6}>
              <Card title="* * * * *">
                <h3>راند دوم</h3>
                <p>فروردین 1400</p>
                <p>راند دوم</p>
                <p>اعلام میگردد</p>
              </Card>
            </Col>
          </Row>
        </div>  
      </ModuleContainer>
      </RTLCard>
      </>      
    )
 }