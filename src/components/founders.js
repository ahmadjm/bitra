import React from "react";
import { Row,Col } from 'antd';
import {ModuleContainer, Header} from "./styled";
import saeed from "../assets/img/managers/saeed.jpg"; 
import hadi from "../assets/img/managers/hadi.jpg"; 
import doktor from "../assets/img/managers/doktor.jpeg"; 
import esmaeel from "../assets/img/managers/esmal.jpg";
import {BitraCard} from "../components/card/card"; 
export const Founders = ()=>{
    return(
        <>
        <Header>
            بنیانگذاران
        </Header>
        <ModuleContainer>
            <Row gutter={12} style={{ margin: "0" }}  className={'flipViewFeature'}  >
              <Col span={6}>
                <BitraCard
                    imgSrc={saeed}
                    style={{height : "350px",width: "200px",padding : "10px"}}
                    title={"مهندس سعید خوشبخت"}
                    description="رئیس هیئت مدیره"
                    width= {"340"}    
                />                 
              </Col>  
              <Col span={6}>
                <BitraCard
                    imgSrc={hadi}
                    style={{height : "350px",width: "200px",padding : "10px"}}
                    title="مهندس هادی غریب"
                    description="عضو هیئت مدیره"
                    width= {"340"}
                />
              </Col>
              <Col span={6} >
                <BitraCard
                    imgSrc={doktor}
                    width= {"340"}
                    title="دکتر آرین امین الرعایا"
                    style={{height : "350px",width: "200px",padding : "10px"}}
                    description="عضو هیئت مدیره"
                />
               </Col>
               <Col span={6}>
                <BitraCard
                    imgSrc={esmaeel}
                    width= {"340"}
                    style={{height : "350px",width: "200px",padding : "10px"}}
                    title="اسماعیل خوشبخت" 
                    description="مدیر عامل" 
                />
               </Col>                     
            </Row>    
        </ModuleContainer>
        </>
    )
} 