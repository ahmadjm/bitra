import styled from "styled-components";
import { Layout, Card, Col, Input,Form ,Button } from "antd";
import LoginBgLeft from "../assets/img/loginBgLeft.svg";
import LoginBgRight from "../assets/img/loginBgright.svg";
import "../assets/css/fontiran.css";
export const RoadmapContent = styled.div`
  position: relative;
  & img {
    @media (min-width: 1090px) {
      width: 1020px;
      height: 300px;
    }
    @media (max-width: 1090px) and (min-width: 780px) {
      width: 780px;
      height: 240px;
    }
    @media (max-width: 780px) and (min-width: 580px) {
      width: 560px;
      height: 180px;
    }
    @media (max-width: 580px) and (min-width: 480px) {
      width: 440px;
      height: 140px;
    }
    @media (max-width: 480px) and (min-width: 340px) {
      width: 330px;
      height: 105px;
    }
    @media (max-width: 340px) {
      width: 280px;
      height: 85px;
    }
  }
  font-size: 14px;
  & div {
    position: absolute;
    @media (max-width: 780px) and (min-width: 580px) {
      display: none;
    }
    @media (max-width: 580px) {
      display: none;
    }
  }
  & .step0 {
    top: 95%;
    right: 0%;
  }
  & .step1 {
    top: -17%;
    right: 11%;
  }
  & .step2 {
    top: 95%;
    right: 26%;
  }
  & .step3 {
    top: -17%;
    right: 35%;
  }
  & .step4 {
    top: 95%;
    right: 47%;
  }
  & .step5 {
    top: -17%;
    right: 63%;
  }
  & .step6 {
    top: 95%;
    right: 79%;
  }
  & .step7 {
    top: -17%;
    right: 87%;
  }
`;
export const SliderTxt = styled.div`
  color: white;
  font-weight: normal;
  font-size: 1em;
  & h3 {
    color: white !important;
    font-weight: bold !important;
    font-size: 1.4em !important;
  }
  text-align: start;
`;
export const SliderTxtEvents = styled(SliderTxt)`
  justify-content: center;
  text-align: start;
  font-size: 0.8em !important;

  & h4 {
    color: white !important;
    font-weight: bold !important;
    font-size: 1em !important;
  }
  & .ant-card-body {
    display: flex !important;
    flex-direction: row !important;
    justify-content: space-around !important;
    align-items: center !important;
  }
  padding: 10px;
`;
export const RTLLayoout = styled(Layout)`
  direction: rtl;
  font-family: "IRANSans";
  & .sticky {
    display: flex;
    flex-direction: row;
    background: rgb(46, 63, 79, 0);
    color: #154add !important;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 999;
  }
  & .sticky4Slide {
    background: rgb(46, 63, 79, 1) !important;
    transition: all 0s ease;
    -webkit-transition: all 0s ease;
    & .ant-menu-item {
      color: white !important;
      transition: all 0s ease;
      -webkit-transition: all 0s ease;
    }
  }
`;
export const FeatCol = styled(Col)`
  background: white;
`;

export const AminCard = styled(Card)`
  border: none;
  padding-top: 35px;
  & > div {
    display: flex;
    flex-direction: row;
    justify-content: center;
    & .ant-card-meta-avatar {
      float: right !important;
    }
  }
  & .ant-card-body {
    display: flex;
    align-items: baseline;
  }
  & .ant-card-grid {
    width: 52%;
  }
  font-size: 1.2em;
`;
export const RTLCard = styled(Card)`
  border: none;
  display: flex;
  flex-direction: row;
  justify-content: center;
  & .ant-card-grid {
    box-shadow: 0 !important;
  }
  & .ant-card-body {
    display: flex;
    align-items: baseline;
    padding: 12px !important;
  }
  & .ant-card-meta-title {
    font-size: 1.3em;
  }
  font-size: 1.2em;
`;
export const Footer = styled(Card)`
  border: none;
  display: flex;
  flex-direction: row;
  justify-content: center;
  & .ant-card-grid {
    box-shadow: 0 !important;
  }
  & .ant-card-body {
    display: flex;
    align-items: baseline;
  }
  & .ant-card-meta-title {
    font-size: 1.3em;
  }
  font-size: 1.2em;
  background: #435469 !important;
  & ul li {
    diplay: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    margin-top: 18px;
  }
`;
export const RTLCard4Slide = styled(RTLCard)`
  background: transparent;
  & .ant-card-body {
    flex-direction: row-reverse;
    justify-content: center;
  }
  & img.sliderImage {
    @media (max-width: 968px) {
      display: none;
    }
  }
`;
export const WhiteTxt = styled.span`
  color: white;
`;
export const SliderContent = styled.h3`
  background-color: #547eeb !important;
  color: "#fff";
  display: flex;
  flex-direction: row;
  justify-content: center;
  & span {
    line-height: 180px;
  }
  margin: 0;
`;
export const Header = styled.div`
  font-size: 1.5em;
  margin: 30px 0 20px 0;
  color: #111;
  line-height: 1;
  font-weight: bold;
`;

export const FeatHeader = styled.div`
  background: white;
  padding: 45px 0 10px 0;
  font-size: 1.5em;
  color: #111;
  line-height: 1;
  font-weight: bold;
`;
export const ModuleContainer = styled.div`
    width: 100%;
    display:flex;
    flex  1;
    justify-content: center;
    margin-top :25px;
    font-weight:350;
    text-align : right;
    flex-direction :row;
    & .flipView .ant-col .ant-card-body{
      justify-content: center !important;
    }
    
    & .flipViewFeature .ant-col{
      @media (max-width: 1480px) and (min-width : 780px) {
          flex: 0 0 50% !important;
          max-width: 50% !important;
        }
        @media (max-width : 780px) {
          flex: 0 0 100% !important;
          max-width: 100% !important;
        }
        @media (min-width : 1480px) {
          flex: 0 0 25% !important;
          max-width: 25% !important;
        }
      & .sideImg {
        @media (min-width : 1280px){
          width: 350px;          
        }
        @media (min-width : 984px) and (max-width : 1280px){
          width: 320px;   
          margin-top : 6%;       
        }
        @media (min-width : 842px) and (max-width : 984px){
          width: 300px;   
          margin-top : 8%;       
        }
        @media (min-width : 680px) and (max-width : 842px){
          width: 280px;   
          margin-top : 12%;       
        }
        @media  (max-width : 680px){
          width: 35%;        
        }
      }
    
      & .sideTxt {
        @media (min-width : 1280px) {
          flex: 1 !important;
          max-width: 80% !important;
        }
      }
      & .AminImage {
        @media (min-width: 1480px) {
          width: 150px;
          height: 150px;
        }
        @media (max-width: 1480px) and (min-width: 980px) {
          width: 120px;
          height: 120px;
        }
        @media (max-width: 980px) {
          width: 100px;
          height: 100px;
        }

      }
    }

    & .flipView .ant-col{
      @media (max-width: 1280px)  and (min-width : 680px) {
        flex: 0 0 50% !important;
        max-width: 50% !important;
      }
      @media (max-width : 680px) {
        flex: 0 0 100% !important;
        max-width: 100% !important;
      }
      @media (min-width : 1280px) {
        flex: 1 !important;
        max-width: 50% !important;
      }
      & .sideImg {
        @media (min-width : 1280px){
          width: 350px;          
        }
        @media (min-width : 984px) and (max-width : 1280px){
          width: 320px;   
          margin-top : 6%;       
        }
        @media (min-width : 842px) and (max-width : 984px){
          width: 300px;   
          margin-top : 8%;       
        }
        @media (min-width : 680px) and (max-width : 842px){
          width: 280px;   
          margin-top : 12%;       
        }
        @media  (max-width : 680px){
          width: 35%;        
        }
      }
    
      & .sideTxt {
        @media (min-width : 1280px) {
          flex: 1 !important;
          max-width: 80% !important;
        }
      }
      & .AminImage {
        @media (min-width: 1480px) {
          width: 150px;
          height: 150px;
        }
        @media (max-width: 1480px) and (min-width: 980px) {
          width: 120px;
          height: 120px;
        }
        @media (max-width: 980px) {
          width: 100px;
          height: 100px;
        }

      }
    }

    & ul {
      list-style-type: none;
    }

`;
export const FlexForFlip = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;

  @media (min-width: 1260px) {
    width: 65vw;
  }
  @media (min-width: 1260px) and (max-width: 1020px) {
    width: 75vw;
  }
  font-size: 1rem;

  & .image {
    @media (min-width: 1480px) {
      width: 250px;
      height: 150px;
    }
    @media (max-width: 1480px) and (min-width: 980px) {
      width: 200px;
      height: 120px;
    }
    @media (max-width: 980px) {
      width: 150px;
      height: 100px;
    }
  }
`;
export const RowFlex = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  alig-items: center;
  width: 80%;
`;
export const LoginContainer = styled.div `
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items : center;
  background-color :#7598ff42;
  background-image: url(${LoginBgLeft}),url(${LoginBgRight});
  background-repeat: no-repeat,no-repeat;
  background-position: top 20px left 30%, top 20px right 30%;
  padding :  85px 0 0 0;
`
export const LoginForm = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 10px;
  -webkit-backdrop-filter: blur(30px);
  backdrop-filter: blur(30px);
  box-shadow: 0 3px 23px 0 rgba(0, 0, 0, 0.2);
  background-color: #fafafa;
  padding: 45px 50px;
  margin : 25px 10px;
  width :450px; 
  
`;

export const Bitrainput = styled(Input)`
  width: 309px;
  margin: 3px 8px;
  padding: 5px 10px;
  -webkit-backdrop-filter: blur(30px);
  backdrop-filter: blur(30px);
  border-radius: 4px;
  border: solid 1px rgba(167, 169, 207, 0.51);
  line-height: 24px;
  & .ant-input{
    color: gray !important;
  }
`;
export const BitraForm = styled(Form)`

`
export const BitraButton = styled(Button)`
  border-radius: 4px;
  box-shadow: -2px 2px 3px 0 rgba(0, 0, 0, 0.1);
  background-color: #0074ff;
  padding: 8px 22px;
  text-align : center;
  width : 103px;
  height : 36px;
  margin-left : 8px;
`