import React from "react";
import { Carousel ,Col} from "antd";
import slider1 from "../assets/img/sliders/slider1.png";
import { Card } from "antd";

import { SliderContent, RTLCard4Slide, SliderTxt } from "../components/styled";

export const Slider = () => {
  const { Meta } = Card;
  return (
    <Carousel autoplay effect={"fade"}>
      <div>
        <SliderContent>
          <RTLCard4Slide hoverable={false} bordered={false}>
            <Col  hoverable={false} style={{ "box-shadow": "none" }}>
                <img alt="بیترا" className={'sliderImage'}  src={slider1}  height="350px" />              
            </Col>
            <Col  hoverable={false} style={{ boxShadow: "none",  margin:'auto' }}>
              <SliderTxt>
                <h3>به بیترا خوش آمدید</h3>
                <h4 style={{color :"white"}} >بیترا سهم تو از آینده</h4>
                <p>
                بازار یکپارچه تبادل رمزارز امین (بیترا) با چشم‌انداز خلق یک کارگزاری غیرمتمرکز رمزارزی پایه‌گذاری شده است
                </p>
              </SliderTxt>
            </Col>
          </RTLCard4Slide>
        </SliderContent>
      </div>
    </Carousel>
  );
};
